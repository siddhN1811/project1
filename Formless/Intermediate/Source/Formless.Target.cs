using UnrealBuildTool;

public class FormlessTarget : TargetRules
{
	public FormlessTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V2;
		Type = TargetType.Game;
		ExtraModuleNames.Add("Formless");
	}
}
